#!/bin/bash

weekdays=("Sunday" "Monday" "Tuesday" "Wednesday" "Thursday" "friday" "saturday")

echo -n "numbers in the set: "
for num in 1 4 5 6 7 8
do 
	echo -n " $num"
done
echo

echo -n "linear numbers: "
for num in {5..10}
do
	echo -n " $num"
done
echo

echo -n "linear numbers: "
for num in {-5..10}
do
        echo -n " $num"
done
echo

echo -n "using seq (2 7 40): "
for num in $(seq 2 7 40)
do
        echo -n " $num"
done
echo

echo -n "elements in weekdays: "
for i in "${weekdays[@]}"
do
        echo -n " $i"
done
echo


