#!/bin/bash

total_N=$#
script_name=$0
N1=$1
N2=$2
N3=$3

function_usage() {
        echo "usage: $script_name <N1> <N2> <N3>"
        echo
}


function_arg_validity() {

        if ! [ $total_N -eq 3 ]; then

                echo "invalid number of arguments"
		function_usage
                exit 1
        fi
}


function_is_num() {

	if [ "$1" -eq "$1" ] 2>/dev/null; then

		return 0
	else
		return 1
	fi
}
function_arg_validity

function_is_num $N1
result=$?

if ! [ $result -eq 0 ]; then
        echo "invalid number"
        exit 1
fi

function_is_num $N2
result=$?

if ! [ $result -eq 0 ]; then
        echo "invalid number"
        exit 1
fi

function_is_num $N3
result=$?

if ! [ $result -eq 0 ]; then
        echo "invalid number"
        exit 1
fi


Result=$(($1+$2+$3))
echo "Total number of terms:$total_N"
echo "N1:$1"
echo "N2:$2"
echo "N3:$3"
echo "Result:$1+$2+$3=$Result"

