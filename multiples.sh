#!/bin/bash


total_N=$#
script_name=$0
N1=$1
N2=$2


function_usage() {


        echo "usage: $script_name <N1> "
        echo "usage: $script_name <N1> <N2>"
}


function_arg_validity() {


       if  [[ $total_N -ne 1 && $total_N -ne 2 ]] ; then

                echo "invalid number of arguments"
                function_usage
		exit 1

       fi
}

function_is_num() {

        if [ "$1" -eq "$1" ] 2>/dev/null; then

                return 0
        else
                return 1
        fi
}

function_arg_validity

function_is_num $N1
result=$?

if ! [ $result -eq 0 ] ; then
	
	echo
	echo "invalid number"
	exit 1

fi

function_is_num $N2
result1=$?

if ! [ $result1 -eq 0 ] ; then
        
	echo
	echo "invalid number"
	exit 1

fi


if [ $total_N -eq 1 ] ; then

        for num in $(seq 10)
        do
                result=$((N1*num))
                echo -n " $result"
       done

elif [ $total_N -eq 2 ] ; then

        for i in $(seq $N2)
        do
                result1=$((N1*i))
                echo -n " $result1"
        done
fi


