#!/bin/bash

bash_var="I am bash variable"

func_print_bash_var() {
	echo $bash_var
}

function func_another_way {
	echo "This is another way to declare function"
}

func_arg_example() {
	total_args=$#
	arg_count=$@
	arg1=$1
	arg2=$2
	echo "Total number of args: $total_args"
	echo "All args: $arg_count"
	echo "Arg1: $arg1"
	echo "Arg2: $arg2"
}

func_int_return_example() {
	return 123
}

func_string_return_example() {
	echo "Success"
}

func_print_bash_var
func_another_way
func_arg_example hello world

func_int_return_example
return_value=$?
echo "Function returned int value: $return_value"

return_value=$(func_string_return_example)
echo "Function returned string value: $return_value"
exit 0
