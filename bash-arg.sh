#!/bin/bash

args_array=("$@")

echo "Total args: $#"
echo "All args: $@"
echo "arg1: $1"
echo "arg2: $2"
echo "command name: $0"
echo "commands array: ${args_array[@]}"
