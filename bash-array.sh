#!/bin/bash

weekdays=("Sunday" "Monday" "Tuesday" "Wednesday" "Thursday" "friday" "saturday")

echo "number of elements in weekdays[]: ${#weekdays[@]}"
echo "contents of weekdays[]: ${weekdays[@]}"
echo "element in weekdays[2]: ${weekdays[2]}"
echo "length of weekdays[2]: ${#weekdays[2]}"
echo
echo

multiples_of_five=(5 10 15 20 25 30 35 40 45 50)
echo "number of elements in multiples_of_five: ${#multiples_of_five[@]}"
echo "contents of multiples_of_five: ${multiples_of_five[@]}"
echo "elements in multiples_of_five[2]: ${multiples_of_five[2]}"
echo "length of multiples_of_five[2]: ${#multiples_of_five[2]}"
echo "slicing first 5 elements of multiples_of_five[]: ${multiples_of_five[@]:0:5}"

copy_of_multiples_of_five=("${multiples_of_five[@]}")
echo "copy of multiples_of_five[]: ${copy_of_multiples_of_five[@]}"

